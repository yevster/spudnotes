#SPuDNotes

##What does it do
SPuDNotes serves two purposes:
1. To create provide a trivially easy way to generate a Notices file from an SPDX document, provided the SPDX document has all the appropriate copyright and licensing information.
2. To demonstrate the use of [SPARQL[(https://www.w3.org/TR/sparql11-overview/) in extracting meaningful data from SPDX. SPuDNotes uses a single SPARQL query to do what it does.

##Known Current Limitations
* Only concluded licenses are used. This may change, pending published guidance from the SPDX Working Group, in consultation with their legal team.
    * The `NOT` operator in license expressions is not translated into text. Once the appropriate guidance for representing it in notices is published by the SPDX Working Group, this operator can be taken into account.
* Only named entities are included. These are limited to:
    * Packages, named by their `spdx:name` property (required)
    * Files which contain an artifactOf attribute whose object contains a `doap:name` property (not required). 

Licenses and copyrights of all other entities are omitted (subject to further advisement with the SPDX working group).

##How to build and run

Java 8 is required.

To build, run `gradlew assemble`.

To run, run `gradlew run`.

To build a distributable archive, run `gradlew distZip`