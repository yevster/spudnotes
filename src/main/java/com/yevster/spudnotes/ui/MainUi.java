package com.yevster.spudnotes.ui;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.yevster.spudnotes.NoticesGenerator;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class MainUi {

    private Pane ui;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField txtPath;

    @FXML
    private Button btnBrowse;

    @FXML
    private Button btnExport;

    @FXML
    private TextArea txtNotices;

    private final FileChooser inputFileChooser;

    private final FileChooser outputFileChooser;

    @FXML
    void initialize() {
        assert txtPath != null : "fx:id=\"txtPath\" was not injected: check your FXML file 'SpudNotes.fxml'.";
        assert btnBrowse != null : "fx:id=\"btnBrowse\" was not injected: check your FXML file 'SpudNotes.fxml'.";
        assert btnExport != null : "fx:id=\"btnExport\" was not injected: check your FXML file 'SpudNotes.fxml'.";
        assert txtNotices != null : "fx:id=\"txtNotices\" was not injected: check your FXML file 'SpudNotes.fxml'.";
        btnBrowse.setOnMouseClicked(this::btnBrowseOnClick);
        //The export button should be enabled if, and only if, there is text to export
        txtNotices.textProperty().addListener((observable, oldValue, newValue) -> {
            btnExport.setDisable(StringUtils.isBlank(newValue));
        });

        btnExport.setOnMouseClicked(this::btnExportOnClick);

    }

    public MainUi() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(
                "SpudNotes.fxml"));
        fxmlLoader.setController(this);

        this.inputFileChooser = new FileChooser();
        inputFileChooser.setTitle("Select SPDX");
        inputFileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("SPDX Files (*.rdf, *.spdx)", "*.spdx", "*.rdf"), new FileChooser.ExtensionFilter("All Files (*.*)", "*.*"));

        this.outputFileChooser = new FileChooser();
        outputFileChooser.setTitle("Export notices file");
        outputFileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Text Files (*.txt)", "*.txt"), new FileChooser.ExtensionFilter("All Files (*.*)", "*.*"));

        try {
            this.ui = fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }


    private void btnBrowseOnClick(Event event) {
        File spdxFile = inputFileChooser.showOpenDialog(ui.getScene().getWindow());
        if (spdxFile == null) {
            txtNotices.setText("");
            return; //Cancelled
        }
        txtPath.setText(spdxFile.getAbsolutePath());
        try {
            txtNotices.setText(NoticesGenerator.generateNotices(spdxFile));
        } catch (IOException e) {
            txtNotices.setText("");
            new Alert(Alert.AlertType.ERROR, "Error generating notices: " + e.getMessage(), ButtonType.OK);
        }
    }

    private void btnExportOnClick(Event event) {
        File outputFile = outputFileChooser.showSaveDialog(ui.getScene().getWindow());
        if (outputFile == null) {
            return;
        }
        try {
            FileUtils.write(outputFile, txtNotices.getText());
        } catch (IOException ioe) {
            new Alert(Alert.AlertType.ERROR, "Error writing notices: " + ioe.getMessage(), ButtonType.OK);
        }
    }

    public Pane getUi() {
        return ui;
    }
}
