package com.yevster.spudnotes;

import com.yevster.spudnotes.ui.MainUi;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by ybronshteyn on 10/30/16.
 */
public class Main extends Application{

    public static final String TITLE = "SpudNotes";

    @Override
    public void start(Stage primaryStage) throws Exception {

        Scene scene = new Scene(new MainUi().getUi());
        primaryStage.setTitle(TITLE);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
