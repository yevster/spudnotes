package com.yevster.spudnotes;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * Created by ybronshteyn on 10/30/16.
 */
public final class NoticesGenerator {

    private static final Query noticesQuery;

    private static final String LICENSE_FIELD = "licTexts";
    private static final String COPYRIGHT_FIELD = "copyrightTexts";
    private static final String NOTICE_FIELD = "noticeText";

    static {
        final String noticesQuerySparql;
        try {
            noticesQuerySparql = IOUtils.toString(NoticesGenerator.class.getClassLoader().getResourceAsStream("queryForNotices.sparql"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        noticesQuery = QueryFactory.create(noticesQuerySparql);

    }

    public static String generateNotices(File spdxFile) throws IOException {
        try (InputStream is = new FileInputStream(spdxFile)) {
            return generateNotices(is);
        }

    }

    public static String generateNotices(InputStream in) {
        //return Long.toString(RandomUtils.nextLong(0,66666));

        Model model = ModelFactory.createDefaultModel();
        model.read(in, null);

        QueryExecution qe = QueryExecutionFactory.create(noticesQuery, model);
        ResultSet results = qe.execSelect();

        StringBuilder result = new StringBuilder();

        while (results.hasNext()) {
            QuerySolution solution = results.next();
            String name = solution.getLiteral("name").getString();
            result.append("\n================================================================\n" + name + "\n");
            if (solution.contains(NOTICE_FIELD) && StringUtils.isNotBlank(solution.getLiteral(NOTICE_FIELD).getString())) {
                result.append(solution.getLiteral(NOTICE_FIELD).getString());
            } else {
                if (solution.get(COPYRIGHT_FIELD) != null && solution.get(COPYRIGHT_FIELD).isLiteral()) {
                    result.append(solution.getLiteral(COPYRIGHT_FIELD).getString());
                    result.append("\n");
                }
                //TODO: Move the blank check to SPARQL
                if (solution.get(LICENSE_FIELD) != null && solution.get(LICENSE_FIELD).isLiteral()) {
                    result.append(solution.getLiteral(LICENSE_FIELD).getString());
                    result.append("\n");
                }
            }

            result.append("\n");
        }

        return result.toString();

    }
}
